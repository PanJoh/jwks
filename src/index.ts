import * as jose from 'jose';
import { createLocalJWKSet, createRemoteJWKSet } from 'jose';
import base64url from 'base64url';
import { GetKeyFunction } from 'jose/dist/types/types';

export abstract class JwkSetBase {
    protected abstract joseJwkSet: ReturnType<typeof createLocalJWKSet>;

    async verify(jws: string) {
        return jose.jwtVerify(jws, this.joseJwkSet)
    }

    async getKey(token: string) {
        const { 0: protHeader, 1: payload, 2: signature, length} = token.split('.');
        if (length !== 3) {
            throw new Error('invalid token');
        }

        const header = JSON.parse(base64url.decode(protHeader));

        return this.joseJwkSet(header, {payload, signature});
    }

    async getPublicKey(token: string) {
        let key = await this.getKey(token);
        if (key instanceof Uint8Array) {
            throw new Error('unsupported key type');
        }
        return jose.exportSPKI(key);
    }
}

type LocalJwkSetOptions = jose.JSONWebKeySet;

export class JwkLocalSet extends JwkSetBase {
    protected joseJwkSet: ReturnType<typeof createLocalJWKSet>;
    constructor(options: LocalJwkSetOptions) {
        super();
        this.joseJwkSet = jose.createLocalJWKSet(options);
    }
}

export class JwkRemoteSet extends JwkSetBase {
    protected joseJwkSet: GetKeyFunction<jose.JWSHeaderParameters, jose.FlattenedJWSInput>;

    constructor(url: URL, options?: jose.RemoteJWKSetOptions) {
        super();
        this.joseJwkSet = createRemoteJWKSet(url, options);
    }
}

type PassportProvider = (req: any, token: string, done: (err: any, secret?: string) => void) => void;

export function createPassportProvider(url: URL, options?: jose.RemoteJWKSetOptions): PassportProvider {
    const keySet = new JwkRemoteSet(url, options);

    return async (_, token, done) =>  {
        try {
            const pem = await keySet.getPublicKey(token);
            done(null, pem);
        } catch (err) {
            done(err);
        }
    }
}
